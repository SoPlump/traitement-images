import cv2
import os
import numpy as np
import argparse
from statistics import mean


parser = argparse.ArgumentParser()
parser.add_argument("imageFolder",help= "Path to the /Images folder")
args = vars(parser.parse_args())
imageFolderPath = args["imageFolder"]



# Function made by Adrian Rosebrock
# https://www.pyimagesearch.com/2015/10/05/opencv-gamma-correction/
def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
        for i in np.arange(0, 256)]).astype("uint8")
	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)


def detect_objects(ref, img, base,  ind = 0):

    ## XOR
    img = cv2.bitwise_xor(img,ref)

    ## MORPH - OPEN
    kernel = np.ones((9, 9), np.uint8)
    img = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)

    ## MORPH - DILATE
    kernel = np.ones((25, 25), np.uint8)
    img = cv2.morphologyEx(img, cv2.MORPH_DILATE, kernel)

    ## Find contours
    contours, hierarchy = cv2.findContours(img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    img = cv2.drawContours(img, contours, -1, (0, 255, 0), 2)

    ##Calculate surface areas mean
    rect_areas = []
    for c in contours:
        (x, y, w, h) = cv2.boundingRect(c)
        rect_areas.append(w * h)
    avg_area = mean(rect_areas)

    ## Draw rectangles around objects
    for i,cnt in enumerate(contours):
        # if the contour has no other contours inside of it
        if hierarchy[0][i][2] == -1 :
            # if the size of the contour is greater than a threshold
            if cv2.contourArea(cnt) > (avg_area * 0.53):
                x, y, w, h = cv2.boundingRect(cnt)
                cv2.rectangle(base, (x, y), (x + w, y + h), (0, 0, 255), 2)
    return img

##
# Cuisine

##LOADING IMAGES

kitchen_original = cv2.imread( imageFolderPath + "\\Cuisine\\Reference.JPG")
kitchen_original = cv2.resize(kitchen_original, (960, 540))
kitchen_original = kitchen_original[250:540, 100:700]
kitchen_ref = cv2.imread(imageFolderPath + "\\Cuisine\\Reference.JPG" , cv2.IMREAD_GRAYSCALE)
kitchen_mask = cv2.imread(imageFolderPath + "\\Cuisine\\Reference_MASK.JPG" , cv2.IMREAD_GRAYSCALE)

##APPLYING MASK
kitchen_ref = cv2.bitwise_and(kitchen_ref,kitchen_ref,mask = kitchen_mask)

##RESIZING
kitchen_ref = cv2.resize(kitchen_ref, (960, 540))
kitchen_ref = kitchen_ref[250:540, 100:700]

##GAUSSIAN BLUR
kitchen_floor = cv2.GaussianBlur(kitchen_ref, (5, 5), 0)

##BINARY THRESHOLD
ret0, kitchen_floor = cv2.threshold(kitchen_floor, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

kitchen_folder_path = (imageFolderPath + "\\Cuisine\\")
images = []
for filename in os.listdir(kitchen_folder_path):
    if "Reference" not in filename:
        img = cv2.imread(os.path.join(kitchen_folder_path, filename),cv2.IMREAD_GRAYSCALE)
        if img is not None:
            images.append(img)

index = 0
for image in images:
    pre_treated_image = image

    ##APPLYING MASK
    pre_treated_image = cv2.bitwise_and(pre_treated_image,pre_treated_image,mask = kitchen_mask)

    ##RESIZING
    pre_treated_image = cv2.resize(pre_treated_image, (960, 540))
    pre_treated_image = pre_treated_image[250:540, 100:700]
    image_resized = pre_treated_image

    ##GAUSSIAN BLUR
    pre_treated_image = cv2.GaussianBlur(pre_treated_image, (5,5),0)

    ##BINARY THRESHOLD
    ret1, pre_treated_binary_image = cv2.threshold(pre_treated_image,0,255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    ##ERODING
    kernel = np.ones((3, 2), np.uint8)
    pre_treated_binary_image = cv2.morphologyEx(pre_treated_binary_image, cv2.MORPH_ERODE, kernel)

    ##RESULTS
    res = detect_objects(kitchen_floor, pre_treated_binary_image, image_resized, index)
    cv2.imshow(f"cuisine res{index}",image_resized)

    index = index + 1



# Living Room ----------------------------------------------------------------------------------------------------------

##LOADING IMAGES

livingRoom_original = cv2.imread(imageFolderPath + "\\Salon\\Reference.JPG")
livingRoom_original = cv2.resize(livingRoom_original, (960, 540))
livingRoom_original_grey = cv2.imread(imageFolderPath + "\\Salon\\Reference.JPG", cv2.IMREAD_GRAYSCALE)
livingRoom_ref = cv2.imread(imageFolderPath + "\\Salon\\Reference.JPG" , cv2.IMREAD_GRAYSCALE)
livingRoom_mask = cv2.imread(imageFolderPath + "\\Salon\\Reference_MASK.JPG" , cv2.IMREAD_GRAYSCALE)

##APPLYING MASK
livingRoom_ref = cv2.bitwise_and(livingRoom_ref,livingRoom_ref,mask = livingRoom_mask)

##RESIZING
livingRoom_ref = cv2.resize(livingRoom_ref, (960, 540))
livingRoom_ref = livingRoom_ref[270:540, 0:960]

## GAUSSIAN BLUR
livingRoom_floor = cv2.GaussianBlur(livingRoom_ref, (13, 13), 0)

##CLAHE
clahe = cv2.createCLAHE(clipLimit=2.0,tileGridSize=(12,12))
# Apply CLAHE to the grayscale image varying clipLimit parameter:
livingRoom_floor = clahe.apply(livingRoom_floor)

##BINARY THRESHOLD
ret0, livingRoom_floor = cv2.threshold(livingRoom_floor, 90, 255, cv2.THRESH_BINARY )

##READ IMAGES
livingRoom_folder_path = (imageFolderPath + "\\Salon\\")
images = []
for filename in os.listdir(livingRoom_folder_path):
    if "Reference" not in filename:
        img = cv2.imread(os.path.join(livingRoom_folder_path, filename),cv2.IMREAD_GRAYSCALE)
        if img is not None:
            images.append(img)

index = 0
for image in images:
    pre_treated_image = image

    ## GAMMA CORRECTION
    pre_treated_image = adjust_gamma(pre_treated_image, gamma=0.90)

    ##MERGING IMAGES
    fusion = cv2.addWeighted(livingRoom_original_grey,0.15,pre_treated_image,0.85,0)
    pre_treated_image = fusion

    ##APPLYING MASK
    pre_treated_image = cv2.bitwise_and(pre_treated_image,pre_treated_image,mask = livingRoom_mask)

    ##RESIZING
    pre_treated_image = cv2.resize(pre_treated_image, (960, 540))
    pre_treated_image = pre_treated_image[270:540, 0:960]
    image_resized = pre_treated_image

    ## GAUSSIAN BLUR
    pre_treated_image = cv2.GaussianBlur(pre_treated_image, (13,13),0)

    ## CLAHE
    clahe = cv2.createCLAHE(clipLimit=2.0,tileGridSize=(12,12))
    pre_treated_image = clahe.apply(pre_treated_image)

    ## BINARY THRESHOLD
    ret1, th1 = cv2.threshold(pre_treated_image, 90, 255, cv2.THRESH_BINARY )
    pre_treated_image = th1

    ## RESULTS
    res = detect_objects(livingRoom_floor,pre_treated_image,image_resized,index)
    cv2.imshow(f"livingRoom res{index}",image_resized)

    index = index + 1



# Room

##LOADING IMAGES

room_mask = cv2.imread(imageFolderPath + "\\Chambre\\Reference_MASK.JPG" , cv2.IMREAD_GRAYSCALE)
room_original = cv2.imread(imageFolderPath + "\\Chambre\\Reference.JPG")
room_original_grey = cv2.imread(imageFolderPath + "\\Chambre\\Reference.JPG", cv2.IMREAD_GRAYSCALE)
room_ref = cv2.imread(imageFolderPath + "\\Chambre\\Reference.JPG" , cv2.IMREAD_GRAYSCALE)

##APPLYING MASK
room_ref = cv2.bitwise_and(room_ref,room_ref,mask = room_mask)

##RESIZING
room_ref = cv2.resize(room_ref, (960, 540))
room_ref = room_ref[100:540, 360:750]

## GAUSSIAN BLUR
room_floor = cv2.GaussianBlur(room_ref, (13, 13), cv2.BORDER_DEFAULT)

##CLAHE
clahe = cv2.createCLAHE(clipLimit=4.0,tileGridSize=(12,12))
# Apply CLAHE to the grayscale image varying clipLimit parameter:
room_floor = clahe.apply(room_floor)

##BINARY THRESHOLD
ret0, room_floor = cv2.threshold(room_floor, 90, 255, cv2.THRESH_BINARY )

##READ TEST IMAGES
room_folder_path = (imageFolderPath + "\\Chambre\\")
images = []
for filename in os.listdir(room_folder_path):
    if "Reference" not in filename:
        img = cv2.imread(os.path.join(room_folder_path, filename),cv2.IMREAD_GRAYSCALE)
        if img is not None:
            images.append(img)

index = 0
for image in images:
    pre_treated_image = image

    ##MERGING IMAGES
    fusion = cv2.addWeighted(room_original_grey,0.25,pre_treated_image,0.75,0)
    pre_treated_image = fusion

    ##APPLYING MASK
    pre_treated_image = cv2.bitwise_and(pre_treated_image,pre_treated_image,mask = room_mask)

    ##RESIZING
    pre_treated_image = cv2.resize(pre_treated_image, (960, 540))
    pre_treated_image = pre_treated_image[100:540, 360:750]
    image_resized = pre_treated_image

    ## CLAHE
    clahe = cv2.createCLAHE(clipLimit=2.0,tileGridSize=(18,18))
    pre_treated_image = clahe.apply(pre_treated_image)

    ## GAUSSIAN BLUR
    pre_treated_image = cv2.GaussianBlur(pre_treated_image, (9,9),cv2.BORDER_DEFAULT)

    ## BINARY THRESHOLD DARK
    ret1, th1 = cv2.threshold(pre_treated_image, 90, 255, cv2.THRESH_BINARY )
    pre_treated_image = th1

    ## RESULTS
    res = detect_objects(room_floor,pre_treated_image,image_resized,index)
    cv2.imshow(f"room res{index}",image_resized)
    index = index + 1


# Nettoyage

cv2.waitKey()



