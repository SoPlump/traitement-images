import numpy as np
import pandas as pd
import tensorflow.keras as tfk
from tensorflow.keras.applications.vgg16 import VGG16
from PIL import Image

input_shape = 32

def load_model():
    isShow = False
    base_model = VGG16(weights="imagenet", include_top=False, input_shape=(input_shape, input_shape, 3))
    base_model.summary()

    # Cloning
    base_model_clone = tfk.models.clone_model(base_model)
    base_model_clone.set_weights(base_model.get_weights())

    # Using layers
    model = tfk.models.Sequential(base_model_clone.layers[:])
    model.add(tfk.layers.Dense(256, activation="relu"))
    model.add(tfk.layers.Dense(26, activation="relu"))
    model.add(tfk.layers.Dense(1, activation="relu"))
    print(model.summary())
    # Freezing all layers but the output
    for layer in model.layers[:-1]:
        layer.trainable = False

    if isShow:
        for i, layer in enumerate(model.layers):
            print(i, layer.name, "-", layer.trainable)

    # Compiling
    model.compile(loss="binary_crossentropy",
                  optimizer="sgd",
                  metrics=["accuracy"])

    return model


def load_train_data(ratio_training=0.8):
    df_train = pd.read_csv("./Images/sign_mnist_train/sign_mnist_train.csv")
    y_labels = df_train.pop('label')
    num_train_images = int(ratio_training * len(y_labels))
    num_valid_images = len(y_labels) - num_train_images

    y_train = y_labels[0: num_train_images]
    y_valid = y_labels[num_train_images:len(y_labels)]

    print(y_labels.shape)
    print(y_train.shape)
    print(y_valid.shape)

    #y_train = tfk.utils.to_categorical(y_train, 26)
    #y_valid = tfk.utils.to_categorical(y_valid, 26)

    final_train_batch = np.zeros([num_train_images, input_shape, input_shape, 3])
    final_valid_batch = np.zeros([num_valid_images, input_shape, input_shape, 3])
    for i in range(0, num_train_images):
        path = "./Images/train-images/image-" + str(i) + ".png"
        im = Image.open(path)
        array = np.asarray(im)
        final_train_batch[i, :, :, 0] = array
        final_train_batch[i, :, :, 1] = array
        final_train_batch[i, :, :, 2] = array

    for i in range(0, num_valid_images):
        path = "./Images/train-images/image-" + str(i + num_valid_images) + ".png"
        im = Image.open(path)
        array = np.asarray(im)
        final_valid_batch[i, :, :, 0] = array
        final_valid_batch[i, :, :, 1] = array
        final_valid_batch[i, :, :, 2] = array

    print(f"Batch train shape : {final_train_batch.shape}")
    print(f"Batch valid shape : {final_valid_batch.shape}")

    return [final_train_batch, y_train, final_valid_batch, y_valid]


def training(model, x_train, y_train, x_valid, y_valid):
    checkpoint_filepath = './checkpoints'

    model_checkpoint_callback = tfk.callbacks.ModelCheckpoint(
        filepath=checkpoint_filepath,
        save_weights_only=True,
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)

    history = model.fit(x_train, y_train, epochs=4,
                        validation_data=(x_valid, y_valid),
                        callbacks=[model_checkpoint_callback])

    for layer in model.layers[:-1]:
        layer.trainable = True

    optimizer = tfk.optimizers.SGD(lr=1e-4)  # the default lr is 1e-2
    model.compile(loss="binary_crossentropy", optimizer=optimizer,
                  metrics=["accuracy"])
    history = model.fit(x_train, y_train, epochs=16,
                        validation_data=(x_valid, y_valid),
                        callbacks=[model_checkpoint_callback])


if __name__ == "__main__":
    [x_train, y_train, x_valid, y_valid] = load_train_data()
    print(f"Old shape : {x_train.shape}")
    x_train = tfk.applications.vgg16.preprocess_input(x_train)
    x_valid = tfk.applications.vgg16.preprocess_input(x_valid)
    print(f"New shape : {x_train.shape}")

    array = x_train[1, :, :, 1]
    print(y_train[1])

    Image.fromarray(array).show()

    model = load_model()
    training(model, x_train, y_train, x_valid, y_valid)
