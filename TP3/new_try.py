# https://www.kaggle.com/dansbecker/transfer-learning

import tensorflow.keras as tfk
from keras_preprocessing.image import ImageDataGenerator
from tensorflow.python.keras.applications.vgg16 import VGG16
from tensorflow.python.keras.applications.vgg16 import preprocess_input

##################
# DATA GENERATOR #
##################

image_size = 224
data_generator = ImageDataGenerator(preprocessing_function=preprocess_input)

train_folder = "C:/Git/traitement-images/TP3/Images/train-images/"
val_folder = "C:/Git/traitement-images/TP3/Images/valid-images/"
test_folder = "C:/Git/traitement-images/TP3/Images/test-images/"

train_generator = data_generator.flow_from_directory(
    train_folder,
    target_size=(image_size, image_size),
    class_mode='categorical',
    shuffle=False
)

validation_generator = data_generator.flow_from_directory(
    val_folder,
    target_size=(image_size, image_size),
    class_mode='categorical',
    shuffle=False
)

test_generator = data_generator.flow_from_directory(
    test_folder,
    target_size=(image_size, image_size),
    class_mode='categorical',
    shuffle=False
)

################
# Create Model #
################

base_model = VGG16()

# Cloning
base_model_clone = tfk.models.clone_model(base_model)
base_model_clone.set_weights(base_model.get_weights())

# Using layers
model = tfk.models.Sequential(base_model_clone.layers[:-1])
model.add(tfk.layers.Dense(256, activation="relu"))
model.add(tfk.layers.Dense(26, activation="relu"))
model.add(tfk.layers.Dense(1, activation="relu"))

# Freezing all layers but the output
for layer in model.layers[:-3]:
    layer.trainable = False

# Compiling
model.compile(loss="binary_crossentropy",
              optimizer="sgd",
              metrics=["accuracy"])

############
# Training #
############
checkpoint_filepath = '/checkpoints'

model_checkpoint_callback = tfk.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath,
    save_weights_only=True,
    monitor='val_accuracy',
    mode='max',
    save_best_only=False)

history = model.fit(train_generator, epochs=4,
                    validation_data=validation_generator,
                    callbacks=[model_checkpoint_callback])

for layer in model.layers[:-1]:
    layer.trainable = True

optimizer = tfk.optimizers.SGD(lr=1e-4)  # the default lr is 1e-2
model.compile(loss="binary_crossentropy", optimizer=optimizer,
              metrics=["accuracy"])
history = model.fit_generator(train_generator, epochs=16,
                              validation_data=validation_generator,
                              callbacks=[model_checkpoint_callback])
