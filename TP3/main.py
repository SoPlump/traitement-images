import tensorflow.keras as tfk
from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg16 import preprocess_input

import pandas as pd
import cv2
import numpy as np

# Note : images are 28*28


def load_train_data():
    df_train = pd.read_csv("./Images/sign_mnist_train/sign_mnist_train.csv")
    df_as_array = df_train.values
    y_train = df_as_array[:, 0]
    x_train = df_as_array[:, 1:len(df_as_array)-1]

    # Creating validation data
    nb_valid = 20 * len(x_train) / 100
    x_valid = x_train[:-int(nb_valid)]
    y_valid = y_train[:-int(nb_valid)]

    x_train = x_train[0: 4*int(nb_valid)]
    y_train = y_train[0: 4*int(nb_valid)]

    return [x_train, y_train, x_valid, y_valid]


def load_test_data():
    df_test = pd.read_csv("./Images/sign_mnist_test/sign_mnist_test.csv")
    df_as_array = df_test.values
    y_test = df_as_array[:, 0]
    x_test = df_as_array[:, 1:len(df_as_array)-1]
    return [x_test, y_test]


def load_model():
    isShow = False
    base_model = VGG16(weights="imagenet")
    base_model.summary()

    # Cloning
    base_model_clone = tfk.models.clone_model(base_model)
    base_model_clone.set_weights(base_model.get_weights())

    # Using layers
    model = tfk.models.Sequential(base_model_clone.layers[:-1])
    model.add(tfk.layers.Dense(24, activation="relu"))

    # Freezing all layers but the output
    for layer in model.layers[:-1]:
        layer.trainable = False

    if isShow:
        for i, layer in enumerate(model.layers):
            print(i, layer.name, "-", layer.trainable)

    # Compiling
    model.compile(loss="binary_crossentropy",
                  optimizer="sgd",
                  metrics=["accuracy"])

    return model

    # Normalement il faut
    # fixer toutes les couches sauf la dernière
    # virer la dernière couche
    # la remplacer par nos labels
    # adapter le réseau à nos images d'entrée (qui diffère des siennes)
    # entraîner le réseau

    # Voilà ça c'est si j'ai bien compris..; je te rejoins demain ^^


def preprocess_data(input_data):
    """
    Resizes and adds channels to the input_data
    :param input_data: data to be preprocessed
    :return: the preprocessed data
    """
    images_reshaped = np.zeros([len(input_data), 32, 32, 3])

    # First transform image to be a x*x image instead of an x²*1
    index = 0
    for image in input_data:
        reshaped_image = np.zeros([28, 28, 3], dtype=np.uint8)
        for i in range(0, len(image)):
            reshaped_image[int(i/28)][i % 28][0:3] = image[i]

        resized_image = cv2.resize(reshaped_image,
                                   dsize=(32, 32),
                                   interpolation=cv2.INTER_CUBIC)

        images_reshaped[index, :, :, :] = resized_image
        # np.insert(images_reshaped, index, resized_image)
        index += 1

    return images_reshaped


def training(model, x_train, y_train, x_valid, y_valid):
    checkpoint_filepath = '/checkpoints'

    model_checkpoint_callback = tfk.callbacks.ModelCheckpoint(
        filepath=checkpoint_filepath,
        save_weights_only=True,
        monitor='val_accuracy',
        mode='max',
        save_best_only=True)

    history = model.fit(x_train, y_train, epochs=4,
                        validation_data=(x_valid, y_valid),
                        callbacks=[model_checkpoint_callback])

    for layer in model.layers[:-1]:
        layer.trainable = True

    optimizer = tfk.optimizers.SGD(lr=1e-4)  # the default lr is 1e-2
    model.compile(loss="binary_crossentropy", optimizer=optimizer,
                  metrics=["accuracy"])
    history = model.fit(x_train, y_train, epochs=16,
                        validation_data=(x_valid, y_valid),
                        callbacks=[model_checkpoint_callback])


if __name__ == "__main__":
    # Training data
    [x_train, y_train, x_valid, y_valid] = load_train_data()

    x_train = tfk.applications.vgg16.preprocess_input(x_train)
    x_valid = tfk.applications.vgg16.preprocess_input(x_valid)

    x_train = preprocess_data(x_train)
    x_valid = preprocess_data(x_valid)

    print(x_train.shape)

    n_classes = 26
    y_train = tfk.utils.to_categorical(y_train, n_classes)
    y_valid = tfk.utils.to_categorical(y_valid, n_classes)

    # Test data
    [x_test, y_test] = load_test_data()

    # Model
    model = load_model()

    # training
    training(model, x_train, y_train, x_valid, y_valid)
