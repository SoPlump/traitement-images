import pandas as pd
import numpy as np

from pathlib import Path
from PIL import Image
import os


def generate_train_images():
    if os.path.exists("./Images/train-images"):
        print("Pass train")
        return
    else:
        Path("./Images/train-images").mkdir(parents=True, exist_ok=True)
        for i in range(26):
            Path("./Images/train-images/" + str(i)
                 ).mkdir(parents=True, exist_ok=True)
        train_img = pd.read_csv(
            "./Images/sign_mnist_train.csv")
        print(train_img.head())

        train_copy = train_img.copy()
        y_train_labels = train_copy.pop('label')
        print(f"labels shape : {y_train_labels.shape}")

        train_copy_array = np.array(train_copy)
        print(train_copy_array)
        print(len(train_copy_array))
        print(len(train_copy_array[0]))

        for i in range(0, len(train_copy_array)):
            image = np.arange(0, len(train_copy_array[0]), 1, np.uint8)
            for j in range(0, len(train_copy_array[0])):
                image[j] = train_copy_array[i, j]
            image_name = "image-" + str(i) + ".png"

            image = np.reshape(image, (28, 28))

            im = Image.fromarray(image)

            resized_image = im.resize((32, 32))

            resized_image.save("./Images/train-images/" +
                               str(y_train_labels[i]) + "/" + image_name)


def generate_test_images():
    if os.path.exists("./Images/test-images"):
        print("Pass test")
        return
    else:
        Path("./Images/test-images").mkdir(parents=True, exist_ok=True)
        for i in range(26):
            Path("./Images/test-images/" + str(i)
                 ).mkdir(parents=True, exist_ok=True)
        test_img = pd.read_csv("./Images/sign_mnist_test.csv")
        print(test_img.head())

        test_copy = test_img.copy()
        y_test_labels = test_copy.pop('label')
        print(f"labels shape : {y_test_labels.shape}")

        test_copy_array = np.array(test_copy)
        print(test_copy_array)
        print(len(test_copy_array))
        print(len(test_copy_array[0]))

        for i in range(0, int(len(test_copy_array) / 2)):
            image = np.arange(0, len(test_copy_array[0]), 1, np.uint8)
            for j in range(0, len(test_copy_array[0])):
                image[j] = test_copy_array[i, j]
            image_name = "image-" + str(i) + ".png"

            image = np.reshape(image, (28, 28))

            im = Image.fromarray(image)

            resized_image = im.resize((32, 32))

            resized_image.save("./Images/test-images/" +
                               str(y_test_labels[i]) + "/" + image_name)


def generate_valid_images():
    if os.path.exists("./Images/valid-images"):
        print("Pass valid")
        return
    else:
        Path("./Images/valid-images").mkdir(parents=True, exist_ok=True)
        for i in range(26):
            Path("./Images/valid-images/" + str(i)
                 ).mkdir(parents=True, exist_ok=True)
        valid_img = pd.read_csv("./Images/sign_mnist_test.csv")
        print(valid_img.head())

        valid_copy = valid_img.copy()
        y_test_labels = valid_copy.pop('label')
        print(f"labels shape : {y_test_labels.shape}")

        valid_copy_array = np.array(valid_copy)
        print(valid_copy_array)
        print(len(valid_copy_array))
        print(len(valid_copy_array[0]))

        for i in range(int(len(valid_copy_array) / 2), len(valid_copy_array)):
            image = np.arange(0, len(valid_copy_array[0]), 1, np.uint8)
            for j in range(0, len(valid_copy_array[0])):
                image[j] = valid_copy_array[i, j]
            image_name = "image-" + str(i) + ".png"

            image = np.reshape(image, (28, 28))

            im = Image.fromarray(image)

            resized_image = im.resize((32, 32))

            resized_image.save("./Images/valid-images/" +
                               str(y_test_labels[i]) + "/" + image_name)


if __name__ == "__main__":
    generate_train_images()
    generate_test_images()
    generate_valid_images()
