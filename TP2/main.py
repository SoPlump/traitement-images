import cv2
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from numpy.lib.type_check import imag
from scipy import ndimage as ndi

from skimage.segmentation import watershed
from skimage.feature import peak_local_max
from skimage.util import img_as_ubyte
from skimage.filters import rank
from skimage.morphology import disk
from skimage.filters import sobel
from skimage import morphology
from skimage.color import label2rgb
from skimage.feature import canny

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from skimage.data import astronaut
from skimage.segmentation import felzenszwalb, slic, quickshift
from skimage.segmentation import mark_boundaries
from skimage.util import img_as_float


index_grain = 0
masks = []
originals = []
luminane_increased_images = []

def open_images():
    global original_images

    for filename in os.listdir(folder_path):
        img = cv2.imread(os.path.join(folder_path, filename))
        if img is not None:
            images.append(img)
            originals.append(img)
            luminane_increased_images.append(img)

    original_images.append(images)


def process_images():
    isShow = False
    index = 0

    # TODO: GARDER UNE COPIE DE L'ORIGINALE POUR AVOIR LES VRAIES COULEURS,

    for i in range(len(luminane_increased_images)):
        ## LUMINANCE
        luminane_increased_image = luminane_increased_images[i]
        luminane_increased_image = cv2.cvtColor(luminane_increased_image, cv2.COLOR_BGR2YUV)
        luminane_increased_image[:,:,0] = cv2.equalizeHist(luminane_increased_image[:,:,0])
        luminane_increased_image = cv2.cvtColor(luminane_increased_image, cv2.COLOR_YUV2BGR)
        luminane_increased_images[i] = luminane_increased_image
        #if isShow:
            #cv2.imshow(f"post equalize {i}", originals[i])
    
    for image in images:
        if isShow:
            cv2.imshow("pre traitement", image)



        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
        if isShow:
            cv2.imshow("post equalize gray", image)

        #BLUR
        image = cv2.GaussianBlur(image,(5,5),0)
        if isShow:
            cv2.imshow("post gaussian blur", image)

        ## CLAHE
        clahe = cv2.createCLAHE(clipLimit=2.0,tileGridSize=(8,8))
        # Apply CLAHE to the grayscale image varying clipLimit parameter:
        image = clahe.apply(image)
        
        if isShow:
            cv2.imshow("post clahe", image)

        ## BINARY
        ret, image = cv2.threshold(image, 96, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        
        if isShow:
            cv2.imshow("post binary", image)

        
        ##OPENING & CLOSING
        kernel = np.ones((9, 9), np.uint8)
        image = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)

        masks.append(image)

        if isShow:
            cv2.imshow("post opening", image)

        ##FILLING CONTOURS
        #contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        #imageNew = image
        #cv2.drawContours(imageNew, contours, -1, (255, 255, 255), -1)
        #print(contours)
        #if isShow:
            #cv2.imshow("post contours", imageNew)

        if isShow:
            cv2.waitKey()
            
        images[index] = image
        index += 1
    


def segment_images():
    for i in range(len(luminane_increased_images)):
        segmentation_felzenszwalb(i)


def label_avg_color(label_field,image):

    out = []
    labels = np.unique(label_field)

    for label in labels:
        mask = (label_field == label).nonzero()
        color = image[mask].mean(axis=0)
        color = [ round(elem, 0) for elem in color ]

        if(color >= [40,40,40]):
            out.append(color)

    return out

def watershed_segmentation():
    isShow = False
    # Now we want to separate the two objects in image
    # Generate the markers as local maxima of the distance to the background
    image = images[0]
    distance = ndi.distance_transform_edt(image)
    local_maxi = peak_local_max(distance, indices=False, footprint=np.ones((3, 3)),
                                labels=image)
    #local_maxi = peak_local_max(distance, indices=False,
                            #labels=image)
    markers = ndi.label(local_maxi)[0]
    labels = watershed(-distance, markers, mask=image)

    fig, axes = plt.subplots(ncols=3, figsize=(8, 2.7), sharex=True, sharey=True,
                            subplot_kw={'adjustable': 'box'})
    ax0, ax1, ax2 = axes

    ax0.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
    ax0.set_title('Overlapping objects')
    ax1.imshow(-distance, cmap=plt.cm.gray, interpolation='nearest')
    ax1.set_title('Distances')
    ax2.imshow(labels,cmap = plt.cm.get_cmap("Spectral"), interpolation='nearest')
    ax2.set_title('Separated objects')
    for ax in axes:
        ax.axis('off')

    fig.tight_layout()
    plt.show()
    if isShow:
        cv2.waitKey()


def watershed_segmentationV2(index = 0):

    masked_imaged = cv2.bitwise_and(luminane_increased_images[index],luminane_increased_images[index], mask=masks[index])
    masked_imaged = cv2.cvtColor(masked_imaged, cv2.COLOR_BGR2GRAY)
    image = img_as_ubyte(masked_imaged)

    # denoise image
    denoised = rank.median(image, disk(2))

    # find continuous region (low gradient -
    # where less than 10 for this image) --> markers
    # disk(5) is used here to get a more smooth image
    markers = rank.gradient(denoised, disk(5)) < 10
    markers = ndi.label(markers)[0]

    # local gradient (disk(2) is used to keep edges thin)
    gradient = rank.gradient(denoised, disk(2))

    # process the watershed
    labels = watershed(gradient, markers)

    # display results
    fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(8, 8),
                             sharex=True, sharey=True)
    ax = axes.ravel()

    ax[0].imshow(image, cmap=plt.cm.gray)
    ax[0].set_title("Original")

    ax[1].imshow(gradient, cmap=plt.cm.nipy_spectral)
    ax[1].set_title("Local Gradient")

    ax[2].imshow(markers, cmap=plt.cm.nipy_spectral)
    ax[2].set_title("Markers")

    ax[3].imshow(image, cmap=plt.cm.gray)
    ax[3].imshow(labels, cmap=plt.cm.nipy_spectral, alpha=.5)
    ax[3].set_title("Segmented")
    #cmap = cm.get_cmap("Spectral")

    for a in ax:
        a.axis('off')

    fig.tight_layout()
    plt.show()

def region_based_segmentation(index = 0):

    masked_imaged = cv2.bitwise_and(luminane_increased_images[index],luminane_increased_images[index], mask=masks[index])
    masked_imaged = cv2.cvtColor(masked_imaged, cv2.COLOR_BGR2GRAY)
    image = masked_imaged
    elevation_map = sobel(image)
    fig, ax = plt.subplots(figsize=(4, 3))
    ax.imshow(elevation_map, cmap=plt.cm.gray, interpolation='nearest')
    ax.axis('off')
    ax.set_title('elevation_map')

    markers = np.zeros_like(image)
    markers[image < 30] = 1
    markers[image > 150] = 2

    fig, ax = plt.subplots(figsize=(4, 3))
    ax.imshow(markers, cmap = plt.cm.get_cmap("Spectral"), interpolation='nearest')
    ax.axis('off')
    ax.set_title('markers')

    segmentation = morphology.watershed(elevation_map, markers)

    fig, ax = plt.subplots(figsize=(4, 3))
    ax.imshow(segmentation, cmap=plt.cm.gray, interpolation='nearest')
    ax.axis('off')
    ax.set_title('segmentation')

    segmentation = ndi.binary_fill_holes(segmentation - 1)
    labeled_coins, _ = ndi.label(segmentation)

    print(labeled_coins.shape)
    print(image.shape[:2])

    image_label_overlay = label2rgb(labeled_coins, image=image)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(6, 3), sharex=True, sharey=True)
    ax1.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
    ax1.contour(segmentation, [0.5], linewidths=1.2, colors='y')
    ax1.axis('off')
    ax1.set_adjustable('box')
    ax2.imshow(image_label_overlay, interpolation='nearest')
    ax2.axis('off')
    ax2.set_adjustable('box')
    plt.show()

def edge_based_segmentation(index = 0):
    masked_imaged = cv2.bitwise_and(luminane_increased_images[index],luminane_increased_images[index], mask=masks[index])
    masked_imaged = cv2.cvtColor(masked_imaged, cv2.COLOR_BGR2GRAY)
    image = masked_imaged

    hist = np.histogram(image, bins=np.arange(0, 256))

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 3))
    ax1.imshow(image, cmap=plt.cm.gray, interpolation='nearest')
    ax1.axis('off')
    ax2.plot(hist[1][:-1], hist[0], lw=2)
    ax2.set_title('histogram of grey values')

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(6, 3), sharex=True, sharey=True)
    ax1.imshow(image > 100, cmap=plt.cm.gray, interpolation='nearest')
    ax1.set_title('coins > 100')
    ax1.axis('off')
    ax1.set_adjustable('box')
    ax2.imshow(image > 150, cmap=plt.cm.gray, interpolation='nearest')
    ax2.set_title('coins > 150')
    ax2.axis('off')
    ax2.set_adjustable('box')
    margins = dict(hspace=0.01, wspace=0.01, top=1, bottom=0, left=0, right=1)
    fig.subplots_adjust(**margins)

    edges = canny(image / 255.)

    fig, ax = plt.subplots(figsize=(4, 3))
    ax.imshow(edges, cmap=plt.cm.gray, interpolation='nearest')
    ax.axis('off')
    ax.set_title('Canny detector')

    fill_coins = ndi.binary_fill_holes(edges)

    fig, ax = plt.subplots(figsize=(4, 3))
    ax.imshow(fill_coins, cmap=plt.cm.gray, interpolation='nearest')
    ax.axis('off')
    ax.set_title('Filling the holes')

    coins_cleaned = morphology.remove_small_objects(fill_coins, 21)

    fig, ax = plt.subplots(figsize=(4, 3))
    ax.imshow(coins_cleaned, cmap=plt.cm.gray, interpolation='nearest')
    ax.axis('off')
    ax.set_title('Removing small objects')



    plt.show()

def segmentation_felzenszwalb(index = 0):
    isShow = True

    global originals

    img = cv2.bitwise_and(luminane_increased_images[index],luminane_increased_images[index], mask=masks[index])
    cv2.imshow(f"originale {index}", originals[index])

    segments_fz = felzenszwalb(img, scale=80, sigma=0.5, min_size=2000)
    image_felzenszwalb_colored = label2rgb(segments_fz,image = img, kind='avg')
    #print(image_felzenszwalb_colored[0,0])

    resultat = label_avg_color(segments_fz,originals[index]) #En BGR
    #print(resultat)
    grains.append(resultat)

    image_felzenszwalb_blue,image_felzenszwalb_red,image_felzenszwalb_green = cv2.split(image_felzenszwalb_colored)

    #segments_slic = slic(img, n_segments=250, compactness=10, sigma=1)
    #segments_quick = quickshift(img, kernel_size=3, max_dist=6, ratio=0.5)

    #print("Felzenszwalb's number of segments: %d" % len(np.unique(segments_fz)))
    #print("Slic number of segments: %d" % len(np.unique(segments_slic)))
    #print("Quickshift number of segments: %d" % len(np.unique(segments_quick)))

    fig, ax = plt.subplots(1, 2, sharex=True, sharey=True,
                           subplot_kw={'adjustable': 'box'})
    fig.set_size_inches(8, 3, forward=True)
    fig.tight_layout()

    ax[0].imshow(mark_boundaries(img, segments_fz))
    ax[0].set_title("Felzenszwalbs's method")
    ax[1].imshow(image_felzenszwalb_colored)
    ax[1].set_title("Coloration")

    for a in ax:
        a.set_xticks(())
        a.set_yticks(())
    plt.show()
    
    if isShow:
        cv2.waitKey()


def create_dataframe():
    global index_grain
    global dataFrame
    data = {
        '': [],
        'Moyenne de B': [],
        'Moyenne de G': [],
        'Moyenne de R': [],
        }

    for grain in grains:
        for color in grain:
            data[''].append("Grain isolé " + str(index_grain))
            data['Moyenne de B'].append(str(color[2]))
            data['Moyenne de G'].append(str(color[1]))
            data['Moyenne de R'].append(str(color[0]))
            index_grain += 1

    df = pd.DataFrame.from_dict(data, orient='columns')

    dataFrame = dataFrame.append(df, ignore_index=True)

if __name__ == '__main__':
    dataFrame  = pd.DataFrame({
                '':  [],
                'Moyenne de B': [],
                'Moyenne de G': [],
                'Moyenne de R': [],})

    parser = argparse.ArgumentParser()
    parser.add_argument("folder_path", help="Path to the Images folder")

    folder_path = vars(parser.parse_args())["folder_path"]
    images = []
    original_images = []
    grains = []
    
    open_images()
    process_images()
    segment_images()
    create_dataframe()
    print(dataFrame)
    cv2.waitKey()
    