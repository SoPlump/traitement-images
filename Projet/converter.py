import os
from xml.dom import minidom
import xml.etree.cElementTree as ET
from PIL import Image, UnidentifiedImageError

ANNOTATIONS_DIR_PREFIX = "C:/Users/sophi/Desktop/train"

DESTINATION_DIR = "C:/Users/sophi/Desktop/train/results"

CLASS_MAPPING = {
    '0': 'no_mask',
    '1': 'mask'
}

index = -1


def formatter(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ")


def create_root(file_prefix, width, height):
    root = ET.Element("annotation")
    ET.SubElement(root, "filename").text = "{}.jpg".format(file_prefix)
    size = ET.SubElement(root, "size")
    ET.SubElement(size, "width").text = str(width)
    ET.SubElement(size, "height").text = str(height)
    ET.SubElement(size, "depth").text = "3"
    return root


def create_object_annotation(root, voc_labels):
    for voc_label in voc_labels:
        obj = ET.SubElement(root, "object")
        ET.SubElement(obj, "name").text = voc_label[0]
        bbox = ET.SubElement(obj, "bndbox")
        ET.SubElement(bbox, "xmin").text = str(voc_label[1])
        ET.SubElement(bbox, "ymin").text = str(voc_label[2])
        ET.SubElement(bbox, "xmax").text = str(voc_label[3])
        ET.SubElement(bbox, "ymax").text = str(voc_label[4])
    return root


def create_file(file_prefix, width, height, voc_labels):
    root = create_root(file_prefix, width, height)
    root = create_object_annotation(root, voc_labels)
    with open("{}/{}.xml".format(DESTINATION_DIR, file_prefix), "w") as f:

        f.write(formatter(root))
        f.close()


def read_file(file_path):
    global index
    file_prefix = file_path.split(".txt")[0]

    # sale sale déso mais je vise l'efficacité là x)
    image_file_name = "{}.jpg".format(file_prefix)
    image_file_path = "{}/{}".format(ANNOTATIONS_DIR_PREFIX, image_file_name)

    if not os.path.exists(image_file_path):
        image_file_name = "{}.jpeg".format(file_prefix)
        image_file_path = "{}/{}".format(ANNOTATIONS_DIR_PREFIX,
                                         image_file_name)
    if not os.path.exists(image_file_path):
        image_file_name = "{}.png".format(file_prefix)
        image_file_path = "{}/{}".format(ANNOTATIONS_DIR_PREFIX,
                                         image_file_name)
        try:
            img = Image.open(image_file_path)
            img = img.convert('RGB')
        except UnidentifiedImageError:
            print("cannot identify image file " + image_file_path)
            return
    else:
        try:
            img = Image.open(image_file_path)
        except UnidentifiedImageError:
            print("cannot identify image file " + image_file_path)
            return

    # Saving image with a simple name : index.jpg
    index += 1
    try:
        file_name = "{}/{}.jpg".format(DESTINATION_DIR, index)
        print(file_name)
        print()
        img.save(file_name)
    except ValueError:
        print("the output format could not be determined from the file name. Use the format option to solve this")
    except OSError:
        print("If the file could not be written. The file may have been created, and may contain partial data.")

    w, h = img.size
    with open(ANNOTATIONS_DIR_PREFIX + "/" + file_path, 'r') as file:
        lines = file.readlines()
        voc_labels = []
        for line in lines:
            voc = []
            line = line.strip()
            data = line.split()
            voc.append(CLASS_MAPPING.get(data[0]))
            bbox_width = float(data[3]) * w
            bbox_height = float(data[4]) * h
            center_x = float(data[1]) * w
            center_y = float(data[2]) * h
            voc.append(round(center_x - (bbox_width / 2)))
            voc.append(round(center_y - (bbox_height / 2)))
            voc.append(round(center_x + (bbox_width / 2)))
            voc.append(round(center_y + (bbox_height / 2)))
            voc_labels.append(voc)
        create_file(index, w, h, voc_labels)


def start():
    if not os.path.exists(DESTINATION_DIR):
        os.makedirs(DESTINATION_DIR)
    for filename in os.listdir(ANNOTATIONS_DIR_PREFIX):
        if filename.endswith('txt'):
            print(filename)
            if not filename.startswith('classes.txt'):
                read_file(filename)
        else:
            print("Skipping file: {}".format(filename))


if __name__ == "__main__":
    start()
