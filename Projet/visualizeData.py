import os
import cv2
import xml.dom.minidom

image_path = "C:/Users/sophi/Desktop/train/results/"
annotation_path = "C:/Users/sophi/Desktop/train/results/"

files_name = os.listdir(image_path)
for filename_ in files_name:
    filename, extension = os.path.splitext(filename_)
    img_path = image_path+filename+'.jpg'
    xml_path = annotation_path+filename+'.xml'
    print(img_path)
    img = cv2.imread(img_path)
    if img is None:
        pass
    dom = xml.dom.minidom.parse(xml_path)
    root = dom.documentElement
    objects = dom.getElementsByTagName("object")
    for object in objects:
        bndbox = object.getElementsByTagName('bndbox')[0]
        name = object.getElementsByTagName('name')[0]
        xmin = bndbox.getElementsByTagName('xmin')[0]
        ymin = bndbox.getElementsByTagName('ymin')[0]
        xmax = bndbox.getElementsByTagName('xmax')[0]
        ymax = bndbox.getElementsByTagName('ymax')[0]
        name_data = name.childNodes[0].data
        xmin_data = xmin.childNodes[0].data
        ymin_data = ymin.childNodes[0].data
        xmax_data = xmax.childNodes[0].data
        ymax_data = ymax.childNodes[0].data

        if name_data == 'mask':
            color = (55, 255, 155)
        else:
            color = (255, 55, 155)

        img = cv2.rectangle(img, (int(xmin_data), int(ymin_data)),
                            (int(xmax_data), int(ymax_data)), color, 5)

        cv2.putText(img, name_data, (int(xmin_data), int(ymin_data)),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, color, 2)
    flag = 0
    flag = cv2.imwrite(
        "C:/Users/sophi/Desktop/train/visualization/{}.jpg".format(filename), img)
    if(flag):
        print(filename, "done")
print("all done ====================================")
